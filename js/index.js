$(function () {
    $("[data-toggle='popover']").popover();
    $("[data-toggle='tooltip']").tooltip();
    $('.carousel').carousel({
        interval: 1500
    });

    $('#contacto').on('show.bs.modal', function (e) {
        console.log('comienza a abrirse');

        $('#contactoBtn').removeClass('btn-outline-success');
        $('#contactoBtn').addClass('btn-secondary');
        $('#contactoBtn').prop('disabled', true);
    });

    $('#contacto').on('shown.bs.modal', function (e) {
        console.log('terminó de abrir');
    });

    $('#contacto').on('hide.bs.modal', function (e) {
        console.log('comienza a ocultarse');

        $('#contactoBtn').removeClass('btn-secondary');
        $('#contactoBtn').addClass('btn-outline-success');
        $('#contactoBtn').prop('disabled', false);
    });

    $('#contacto').on('hidden.bs.modal', function (e) {
        console.log('terminó de ocultar');
    });
})